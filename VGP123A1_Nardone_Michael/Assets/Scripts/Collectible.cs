﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

    //public float lifeTime;

	// Use this for initialization
	void Start () {

        // In the game, collectibles all have a life time of about 5-10 seconds
        // Generally speaking, they appear through some player interaction.
        // -Moving through an invisible trigger
        // -Destroying a monster, destructible block, chest
        // -Putting specific game objects (ie, the armoured knights' heads in lvl2)
        //lifeTime = 10;

        //Destroy(gameObject, lifeTime);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D (Collision2D collider)
    {
        // If the collectible every collides with something, which calls this function, destroy it!
        Destroy(gameObject);
    }
}
