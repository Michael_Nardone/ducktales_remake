﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    static SoundManager _instance = null;

    public AudioSource sfxSource;
    public AudioSource musicSource;

	// Use this for initialization
	void Start () {
        /// PEW PEW PEW

        if(_instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
	
	}
	
	public void playSFX(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }

    public void playMusic(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.Play();
    }

    public static SoundManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }
}
