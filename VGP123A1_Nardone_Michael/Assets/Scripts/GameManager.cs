﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    static GameManager _instance = null;

    //int _life;
    //int _score;

    public GameObject playerPrefab;

	// Use this for initialization
	void Start () {

        if (_instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
	
	}

    public static GameManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }

    //public int score
    //{
    //    get { return _score; }
    //    set { _score = value; }
    //}

    //public int lives
    //{
    //    get { return _life; }
    //    set { _life = value; }
    //}
}
