﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    // Reference for the Rigidbody2D in the script
    Rigidbody2D rb;

    // Character SPEED variable
    public float speed;                 // Accessible through Inspector, determines character movement speed

    // Character JUMP variables
    public float jumpForce;             // Modifies the height of a jump, default value of 0
    bool isGrounded;                    // True if the empty object groundCheck is within groundCheckRadius of isGroundLayer, false otherwise
    public LayerMask isGroundLayer;     // Stores what is considered the ground, what the character can jump from
    public Transform groundCheck;       // Empty game object used to check if the character is touching the ground
    public float groundCheckRadius;     // The radius of circle which decides isGrounded, via groundCheck object, default value of 0

    // Character BOUNCE variables, will use false value of isGrounded to decide if bouncing can be activated
    public float bounceForce;           // Modifies the height of a bounce, default value of 0
    bool isBouncing;                    // True if character is bouncing, necessary to prevent player damage from traps and monsters, default value of false

    // Character PUTT variables
    bool canPutt;                       // True if the character is able to execute PUTT attack, default value of 0
    public Transform puttCheck;         // Empty game object used to check if character is beside solid object
    public float puttCheckRadius;       // The radius of the circle attached to empty game object puttCheck, default value of 0

    // Character DUCK variables
    bool ducking;                       // Sets the animation if the player is ducking or not

    // Character animation variable
    Animator anim;

    // Determines the direction the character will face, defaults value of false
    bool isFacingLeft;                  // Sets the character's sprite scale to positive or negative, determining the direction the sprite faces

    // Character climbing variables
    public bool isOnVine;                      // Determines if the character is within the collision box of a vine, default to false
    public bool isClimbing;                    // Determines if the character is trying to climb the vine, sets the animation, default to false
    float vinePositionX;                // Stores the current X position of the vine the character is trying to climb, used to centre the player on the vine, default to 0
    public float climbSpeed;            // Determines the speed of climbing a vine, default to 0

    // Character being damaged variable
    bool isDamaged;             // Used to set the animation when the character is damaged by a monster or spike traps

	// Use this for initialization
	void Start () {

        // Reference the RigidBody2D in the script
        rb = GetComponent<Rigidbody2D>();

        // Reference the animator component in the script
        anim = GetComponent<Animator>();

        // Check if RigidBody2D initialized correctly
        if (!rb)
        {
            Debug.LogError("No RigidBody2D detected on character.");
        }

        // Check if speed has a default value, if not set in Inspector
        if (speed == 0)
        {
            // Assign a value to speed
            speed = 1.0f;
            Debug.LogWarning("Speed set to " + speed);
        }

        // Check if jumpForce has a default value, if not set in Inspector
        if (jumpForce == 0)
        {
            // Assign a value to jumpForce
            jumpForce = 3.5f;
            Debug.LogWarning("Jump Force set to " + jumpForce);
        }

        // Check if groundCheck is connected to character
        if (!groundCheck)
        {
            Debug.LogError("No groundCheck detected on character.");
        }

        // Check if groundCheck radius has a default value, if not set in Inspector
        if (groundCheckRadius == 0)
        {
            // Assign a value to groundCheckRadius
            groundCheckRadius = 0.01f;
            Debug.LogWarning("Ground Check Radius set to " + groundCheckRadius);
        }

        // Check if bounceForce has default value, if not set in Inspector
        if (bounceForce == 0)
        {
            // Assign a value to bounceForce
            bounceForce = 4.0f;
            Debug.LogWarning("Bounce Force set to " + bounceForce);
        }

        // Check if puttCheck is connected to character
        if (!puttCheck)
        {
            Debug.LogError("No puttCheck detected on character.");
        }

        // Check if puttCheckRadius has a default value, if not set in Inspector
        if (puttCheckRadius == 0)
        {
            // Assign a value to puttCheckRadius
            puttCheckRadius = 0.05f;
            Debug.LogWarning("Putt Check Radius set to " + puttCheckRadius);
        }

        // Check if climbSpeed has a default value, if not set in Inspector
        if (climbSpeed == 0)
        {
            // Assign a value to climbSpeed
            climbSpeed = 1f;
            Debug.LogWarning("Climb speed set to " + climbSpeed);
        }

        // Check if Animator component initialized correctly
        if (!anim)
        {
            Debug.LogError("No Animator detected on character.");
        }
	
	}
	
	// Update is called once every frame
	void Update () {

        /// SET-UP CONTROL VARIABLES ///
        
        // Check if groundCheck object is within groundCheckRadius of layers in isGroundLayer
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);

        // Check if puttCheck object is within puttCheckRadius of layers in isGroundLayer
        canPutt = Physics2D.OverlapCircle(puttCheck.position, puttCheckRadius, isGroundLayer);

        // Checks if user inputs control for LEFT (arrow or A) or RIGHT (arrow or D) movement
        // Returns -1 (left movement), 0 (no movement), or 1 (right movement)
        // Used GetAxisRaw() because game has no acceleration or variable movement speed
        float moveValue = Input.GetAxisRaw("Horizontal");

        // True if DUCK control is used (negative Vertical), GetAxisRaw() returns -1, 0, or 1, depends on input
        float verticalMove = Input.GetAxisRaw("Vertical");

        // Check if the user inputs control for JUMP (Space)
        // Returns true if user is, false otherwise
        bool jump = Input.GetButtonDown("Jump");

        // Check if the user inputs and holds the control for FIRE1 (Mouse 0)
        // Returns true if user is, false otherwise
        bool fire = Input.GetMouseButton(0);

        /// CLIMBING MECHANIC ///
        /// -Character must be overlapping a Vine trigger collider
        /// -User must then input UP control to attach to the Vine
        /// 
        /// BUG: In forest area with lots of vines near each other, isOnVine does not become true quickly enough after leaving one vine for another.
        ///      This causes some vines to be "missed" if the user does not input UP while isOnVine is still true.
        ///      Possible solution: isOnVine false only becomes true after a short frame count

        // If the character has entered any VineLadders triggers and is pressing UP control
        if (isOnVine && verticalMove == 1)
        {
            Debug.LogWarning("Trying to climb a vine!");
            // Toggle the ability to climb
            isClimbing = true;
            // Remove any current velocity the character has
            rb.velocity = new Vector2(0, 0);
            // Move the character to the centre of the vine
            rb.position = new Vector2(vinePositionX, rb.position.y);
        }

        // If the character is overlapped a vine and pressed the up control at least once to attach to the vine
        if (isClimbing)
        {
            // Prevent character from moving left or right while climbing
            moveValue = 0f;
            // Toggle gravity off
            rb.gravityScale = 0;

            // If the user is inputting UP or DOWN, give the character a velocity
            if (verticalMove != 0)
            {
                rb.velocity = new Vector2(0, verticalMove * climbSpeed);
            }
            else
            {
                rb.velocity = new Vector2(0, 0);        // Must remove velocity if the player is not inputting UP or DOWN, because there is no gravity
            }
        }

        /// DUCKING MECHANIC ///
        /// Character must be on the ground, and not moving
        /// Character must stop ducking before doing anything else

        // If the character is currently on the ground and the user is inputting DOWN controls
        if ((verticalMove < 0) && isGrounded)
        {
            Debug.Log("I am ducking here!");
            // Set the variable which speaks to the animator
            ducking = true;
            // Remove any horizontal movement, this is done here because moveValue is set above, but used for horizontal movement below
            // This sort of thing is probably solvable by using more functions instead of a giant main
            moveValue = 0f;
            // Remove the ability to jump, same reason as above
            jump = false;
        }
        else
        {
            ducking = false;    // While the controls are held, ducking is true, cannot be set as false in each frame so must be checked in a conditional
        }

        /// BASIC CHARACTER MOVEMENT ///

        // Apply movement to character through the Rigidbody2D using moveValue
        // -maintains vertical velocity, allows mid-air direction
        if (moveValue != 0)
        {
            Debug.Log("Character moving left or right."); 
        }
        // Set the characters horizontal velocity, vertical velocity remains the same
        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);

        // Change the direction the character is facing by checking the Input.GetAxisRaw value (-1, 0, or 1)
        // 1st condition ==> moving right AND isFacingLeft currently true || 2nd condition ==> moving left AND isFacingLeft currently false
        if ((moveValue > 0 && isFacingLeft) || (moveValue < 0 && !isFacingLeft))
        {
            // Call the toggle function flipFace() to change face direction
            flipFace();
        }

        /// JUMP MECHANIC ///

        // Check if user inputs JUMP control (space)
        if (jump)
        {
            // if the object groundCheck is within groundCheckRadius of a ground layer
            if (isGrounded)
            {
                // Log a message to notify jump success
                Debug.Log("Jump up!");
                // Apply a force to character Rigidbody2D, change amount via jumpForce
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }

            // If the user is currently attached to a vine
            if (isClimbing)
            {
                Debug.Log("Jump off!");
                // Detached the user from the vine
                isClimbing = false;
                // Toggle gravity back on
                rb.gravityScale = 1;
            }
        }
        
        /// BOUNCE MECHANIC ///
        /// Use Down control and Fire1 control, while in the air, to begin bouncing
        /// If bouncing while hitting the ground, continue bouncing

        // Bounce_hit Animation needs Entry/Exit time and Duration, should only occur when coming in contact with something

        // NEED COMMENTS - if you start bouncing mechanic
        // if if character has Y velocity, is holding down button, holding fire button
        if ((rb.velocity.y != 0) && verticalMove < 0 && fire)
        {
            Debug.Log("Commence Bouncing!");
            // Used to set the animation and determine interaction with monsters and destructibles
            isBouncing = true;
            // Prevent the character from immediately ducking upon isGrounded = true
            ducking = false;
        }
        else
        {
            isBouncing = false;
        }
        
        // NEED COMMENTS - if you hit the ground while bouncing
        // if the character has negative Y velocity, holding down button, holding fire button
        if (isBouncing && isGrounded && rb.velocity.y < 0)
        {
            Debug.Log("Bounce me up, Scroogy!");
            //rb.AddForce(Vector2.up * bounceForce, ForceMode2D.Impulse); //---- AddForce is totally messed up. Very inconsistent!
            rb.velocity = new Vector2(rb.velocity.x, bounceForce);
        }

        /// PUTT MECHANIC ///
        /// Use Horizontal controls to move into a solid object
        /// Use Fire1 control to execute putt
        /// Success on destructible objects + tree stumps
        /// Failure on solids

        // Check if user inputs PUTT controls (left/right) and is beside a solid object
        if (moveValue != 0 && canPutt)
        {
            Debug.Log("Setting up the putt.");
            // Check if user then inputs fire1 control (mouse 0), to execute putt
            if (Input.GetButtonDown("Fire1"))
            {
                Debug.Log("Putting for birdie.");
                // This is where a bool will be set to send to an animator controller parameter
            }
        }

        /// TAKING DAMAGE ///
        /// -Occurs if character collides with a Trap or Monster
        /// -Bounces the character up, and in the opposite direction they were currently moving

        //if (isDamaged)
        //{
        //    
        //    rb.AddForce(new Vector2(-rb.velocity.x, 1f), ForceMode2D.Impulse);
        //}

        /// ANIMATOR CONTROLLER ///

        // Sets the Animator Controller parameter, MoveValue, to the float moveValue, from Input.GetAxisRaw("Horizontal")
        // -Determined by the user inputting LEFT or RIGHT controls
        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));

        // Sets the Animator Controller parameter, isGrounded, to true or false, based on isGrounded variable in script
        // -Determined by object groundCheck and overlap with colliders
        anim.SetBool("isGrounded", isGrounded);

        // Sets the Animator Controller parameter to the absolute value of ducking
        // -Based on conditional, will only equal to 1
        anim.SetBool("Ducking", ducking);

        // Sets the Animator Controller parameter Bouncing to the value of bool isBouncing
        // -Based on conditional which checks if character is isGrounded, is holding [Mouse 0] and [Down]
        anim.SetBool("Bouncing", isBouncing);

        // Sets the Animator Controller parameter Climbing to the bool isClimbing
        // -Based on whether or not the character is attached to a vine object
        anim.SetBool("Climbing", isClimbing);
        anim.SetFloat("VerticalMove", Mathf.Abs(verticalMove));

        //
        //anim.SetBool("Damaged", isDamaged);
    }

    // Toggle function, used to switch direction character is facing.
    // Called by conditional in FixedUpdate(), in Basic Movement section
    void flipFace()
    {
        // Toggles the bool isFacingLeft between true and false values.
        isFacingLeft = !isFacingLeft;

        // Create a local variable and set the value to the current localScale value (1 or -1)
        Vector3 scaleFactor = transform.localScale;

        // Set scaleFactor to a 'flipped' value (-1 to 1, and 1 to -1)
        scaleFactor.x *= -1;

        // Update the characters localScale with the new 'flipped' value
        transform.localScale = scaleFactor;
    }

    // This function is called if the character collides with one of the trigger colliders
    void OnTriggerEnter2D(Collider2D trigger)
    {
        string parentName = trigger.transform.parent.name;
        if (parentName == "VineLadders")
        {
            Debug.Log("Vine Ladder enter.");
            // Let the script know the character is within range of the vine
            isOnVine = true;
            // Let the script know the X position of the vine
            vinePositionX = trigger.transform.position.x;
        }
        else if (parentName == "Traps")
        {
            Debug.Log("That hurts!");
            // Let the script know a spike trap has been entered
            isDamaged = true;
        }
    }

    void OnTriggerExit2D(Collider2D trigger)
    {
        if (trigger.transform.parent.name == "VineLadders")
        {
            Debug.Log("Vine ladder exit.");
            // Let the script know the player is no longer in range of the vine
            isOnVine = false;
        }
    }
}
